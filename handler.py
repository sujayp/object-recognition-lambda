import cv2
import boto3
import logging
import numpy as np

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def run(event, context):

	logger.info(event)
	for message in event['Records']:
		bucket_name = message['s3']['bucket']['name']
		key_path = (message['s3']['object']['key'])
		key_name = key_path.split("/")[-1]
		download_from_s3(bucket_name, key_path, key_name)
		detect_image = False
		if("jpg" in key_name):
			image = cv2.imread("/tmp/"+key_name)
			Width = image.shape[1]
			Height = image.shape[0]
			detect_image = True
		else:
			cap = cv2.VideoCapture("/tmp/"+key_name)
			vid_writer = cv2.VideoWriter("/tmp/"+key_name, cv2.VideoWriter_fourcc('M','J','P','G'), 30, (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)) ))
		scale = 0.00392
		classes = None
		with open("yolov3.txt", 'r') as f:
			classes = [line.strip() for line in f.readlines()]
		COLORS = np.random.uniform(0, 255, size=(len(classes), 3))
		logger.info("Downloading Weights")
		download_from_s3(bucket_name, "config/yolov3.weights", "yolov3.weights")
		net = cv2.dnn.readNet("/tmp/yolov3.weights", "yolov3.cfg")
		net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
		net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)
		frameNum = 0
		while(cv2.waitKey(1) < 0):
			if(detect_image and frameNum >0):
				cv2.waitKey(3000)
				break
			elif not detect_image:
				hasFrame, image = cap.read()
				Width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
				Height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
				# if frameNum == 2:
				# 	frameNum = 0
				# 	continue
				if not hasFrame:
					cv2.waitKey(3000)
					cap.release()
					break
			frameNum=frameNum+1
			if frameNum == 30:
				break
			blob = cv2.dnn.blobFromImage(image, scale, (416,416), (0,0,0), True, crop=False)
			net.setInput(blob)
			outs = net.forward(get_output_layers(net))
			class_ids = []
			confidences = []
			boxes = []
			conf_threshold = 0.5
			nms_threshold = 0.4
			for out in outs:
				for detection in out:
					scores = detection[5:]
					class_id = np.argmax(scores)
					confidence = scores[class_id]
					if(confidence > 0.0):
						print("{}:{}".format(classes[class_id], confidence))
							
					if confidence > 0.5:
						center_x = int(detection[0] * Width)
						center_y = int(detection[1] * Height)
						w = int(detection[2] * Width)
						h = int(detection[3] * Height)
						x = center_x - w / 2
						y = center_y - h / 2
						class_ids.append(class_id)
						confidences.append(float(confidence))
						boxes.append([x, y, w, h])
			indices = cv2.dnn.NMSBoxes(boxes, confidences, conf_threshold, nms_threshold)
			for i in indices:
					i = i[0]
					box = boxes[i]
					x = box[0]
					y = box[1]
					w = box[2]
					h = box[3]
					draw_prediction(image, class_ids[i], confidences[i], int(x), int(y), int(x+w), int(y+h), classes, COLORS)
			# t, _ = net.getPerfProfile()
			# label = 'Inference time: %.2f ms' % (t * 1000.0 / cv2.getTickFrequency())
			# print(label)

			if detect_image:
				cv2.imwrite("/tmp/"+key_name, image)
			else:
				vid_writer.write(image.astype(np.uint8))
		cv2.destroyAllWindows()
		upload_to_s3(bucket_name, key_name, "predicted_data/predicted_"+key_name)

def get_output_layers(net):
	layer_names = net.getLayerNames()    
	output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
	return output_layers

def draw_prediction(img, class_id, confidence, x, y, x_plus_w, y_plus_h, classes, COLORS):
	label = str(classes[class_id])
	print(label)
	color = COLORS[class_id]
	cv2.rectangle(img, (int(x), int(y)), (int(x_plus_w), int(y_plus_h)), color, 2)
	#print(x, y, x_plus_w, y_plus_h)
	cv2.putText(img, label, (x-10,y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

def download_from_s3(bucket_name,key_name,output_name):
	logger.info("Downloading from S3")
	s3_client = boto3.resource('s3')
	s3_client.Bucket(bucket_name).download_file(key_name, "/tmp/"+output_name)
	logger.info("Downloaded from S3")

def upload_to_s3(bucket_name,key_name,output_name):
	logger.info("Uploading to S3")
	s3 = boto3.client('s3')
	s3.upload_file("/tmp/"+key_name, bucket_name, output_name)
	logger.info("Uploaded to S3")